﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; // lisasin, et saaks all öelda [key]

namespace EsimeneMVC.Models
{
    public class Inimene
    {

       [Key]public int Nr { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}