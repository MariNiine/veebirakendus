﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models; // lisasin et saaks modelit kasutada

namespace EsimeneMVC.Controllers
{
    public class InimeneController : Controller
    {

       static Dictionary<int, Inimene> inimesed = new Dictionary<int, Inimene>(); // tegin juurde

        static InimeneController() // 
        {
            Inimene mari = new Inimene { Nimi = "Mari", Vanus = 28, Nr = 1 };

            inimesed.Add(mari.Nr, mari);
        }

        // GET: Inimene
        public ActionResult Index()
        {

            return View(inimesed.Values); // Kirjutan esimese asja
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            return View(inimesed[id]); // ka muutsin
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(Inimene inimene)
        {
            try
            {
                // TODO: Add insert logic here
                //Inimene i = new Inimene
                //{
                //    Nimi = collection["Nimi"],
                //    Vanus = int.Parse(collection["Vanus"]),
                //    Nr = inimesed.Values.Max (x => x.Nr)+1
                //};

                inimesed.Add(inimene.Nr, inimene);

                inimene.Nr = inimesed.Values.Max(x => x.Nr) + 1; // inimene saab nri külge
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
